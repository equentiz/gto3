﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketScript : MonoBehaviour {

    public int damage = 1;


    void OnCollisionEnter(Collision collision)
    {
        //direct hit, does more dmg
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<HealthScript>().Damage(damage);
        }
        else if(collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Platform")
        {
            //TODO:: explosion in a radius damaging players
            
        }
        Destroy(this.gameObject);
    }
}
