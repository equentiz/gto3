﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour {


    public int Health = 5;

	
    public void Damage(int damage)
    {
        Health -= damage;
        Debug.Log(Health);
    }
}
