﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {

    private PlayerInputBehaviour playerIB;
    private Rigidbody playerRB;
    public Transform targetObject;
    private Vector3 rotationMaskLeft = new Vector3(0, -1, 0);
    private Vector3 rotationMaskRight = new Vector3(0, 1, 0);
    public float Jumpforce = 10f;
    public float MovementSpeed = 5.0f;
    private float groundDistance = 0.3f;

	void Start ()
    {
        playerIB = GetComponent<PlayerInputBehaviour>();
        playerRB = GetComponent<Rigidbody>();
    }

    public void Jump()
    {
        if (isGrounded())
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().AddForce(Vector3.up * Jumpforce, ForceMode.Impulse);
        }
    }

    public void MoveLeft()
    {
        GetComponent<Transform>().RotateAround(targetObject.transform.position,
                rotationMaskLeft, MovementSpeed * Time.deltaTime);
    }

    public void MoveRight()
    {
        transform.RotateAround(targetObject.transform.position,
              rotationMaskRight, MovementSpeed * Time.deltaTime);
    }

    bool isGrounded()
    {
        Debug.Log(groundDistance + GetComponent<Transform>().localScale.y);
        return Physics.Raycast(GetComponent<Transform>().position,
            Vector3.down, groundDistance + GetComponent<Transform>().localScale.y/2);
    }

    void Update()
    {
        if (playerIB.JumpButtonPressed())
        {
            Jump();
        }
        if (playerIB.MoveLeftButtonPressed())
        {
            MoveLeft();
        }
        if (playerIB.MoveRightButtonPressed())
        {
            MoveRight();
        }
        if (playerIB.Touch())
        {
            Vector2 movementDirection = playerIB.GetMovementDirectionTouch() * MovementSpeed;
            if(movementDirection.x > 0)
            {
                MoveLeft();
            }
            else if(movementDirection.x < 0)
            {
                MoveRight();
            }
        }
        lookAtTarget();
    }

    void lookAtTarget()
    {
        this.gameObject.GetComponent<Transform>().LookAt(new Vector3(targetObject.GetComponentInParent<Transform>().position.x,                                                        targetObject.GetComponentInParent<Transform>().position.z));
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(targetObject.GetComponent<Transform>().position, 5);
        Gizmos.DrawLine(GetComponent<Transform>().position, new Vector3(GetComponent<Transform>().position.x,
            GetComponent<Transform>().position.y - 1.2f, GetComponent<Transform>().position.z));
    }

    Vector3 GetDirection()
    {
        Vector3 heading =  transform.position - targetObject.position;
        float distance = heading.magnitude;
        Vector3 direction = heading / distance;
        return direction.normalized;
    }
}
