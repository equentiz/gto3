﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpawnLocationManager : MonoBehaviour {

    Random random;
    public GameObject Boss;
    public GameObject Player;
    public GameObject GroundBlockPrefab;
    public List<Vector3> possibleSpawnLocations = new List<Vector3>();
    public int NumberOfLocations = 360;

    void Start()
    {
        CalculatePossibleSpawnLocations();
        
    }

    /// <summary>
    /// Calculate all the given spawnlocations based on the distance between the player and the boss and the numberoflocations requested
    /// it will also place a groundblockprefab on each location.
    /// </summary>
    public void CalculatePossibleSpawnLocations()
    {
        float radius = Vector3.Distance(Boss.transform.position, Player.transform.position);
        
        for(int i = 0; i != NumberOfLocations; i++)
        {
            float angle = i * Mathf.PI * 2f / NumberOfLocations;
            Vector3 newpos = new Vector3(Mathf.Cos(angle) * radius, 0.5f, Mathf.Sin(angle) * radius);
            GameObject go = Instantiate(GroundBlockPrefab, newpos, Quaternion.identity);
            if(i == 0 || i == 1 || i == 2) { go.GetComponent<Renderer>().material.color = Color.black; }
            go.transform.LookAt(new Vector3(Boss.transform.position.x, go.transform.position.y, Boss.transform.position.z));
            possibleSpawnLocations.Add(newpos);
        }
    }

    /// <summary>
    /// Retrieves a random spot from one of the possible spawn locations within the given range
    /// </summary>
    /// <param name="min">minimum </param>
    /// <param name="max"></param>
    /// <returns></returns>
    public Vector3 GetRandomSpot(int min, int max)
    {
        return possibleSpawnLocations[Random.Range(min, max)];
    }
}
