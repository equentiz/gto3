﻿using UnityEngine;
using System.Collections;

public class PlayerInputBehaviour : MonoBehaviour
{
    public Joystick joyStick;

    public bool Touch()
    {
        if(joyStick.Direction != null)
        {
            return true;
        }
        return false;
    }

    public Vector2 GetMovementDirectionTouch()
    {
        Vector2 direction = joyStick.Direction;
        return direction;
    }

    public bool JumpButtonPressed()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.UpArrow))
        {
            return true;
        }
        return false;
    }
    public bool MoveLeftButtonPressed()
    {
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            return true;
        }
        return false;
    }

    public bool MoveRightButtonPressed()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            return true;
        }
        return false;
    }
}
