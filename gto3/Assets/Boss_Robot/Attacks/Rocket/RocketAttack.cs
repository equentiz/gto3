﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketAttack : MonoBehaviour {

    public float Delay = 1;
    public Transform cameraPositionNormal;
    public Transform cameraPositionBack;
    public int nrOfRockets;
    public int nrofrockets2;
    public float interval;
    public RocketAttackTarget attackType;
    public GameObject rocket;
    public GameObject fakerocket;
    public GameObject playerTarget;
    public GameObject boss;
    public float RocketForceUp;
    public float RocketGravityMultiplier;
    public List<Transform> smallRocketLaunchers;
    Vector3 spawn;
    int smliterator;
    Camera c;

    void Start()
    {
        playerTarget = GameObject.FindGameObjectWithTag("Player");
        c = Camera.main;
        boss = GameObject.FindGameObjectWithTag("Boss");
    }

    void Update()
    {
        if(nrOfRockets == 0)
        {
            Invoke("delayedCamReturn", 3);
            
        }
    }

    void delayedCamReturn()
    {
       c.transform.SetPositionAndRotation(cameraPositionNormal.position, c.transform.rotation); //move to camera script
    
    }

    

    public void BeginAttack()
    {
        c = Camera.main;
        c.transform.SetPositionAndRotation(cameraPositionBack.position, c.transform.rotation); // move to a camera script
        if (attackType == RocketAttackTarget.PLAYER)
        {
            nrofrockets2 = nrOfRockets;
            LaunchRockets();
            
            InvokeRepeating("FireRocketPlayer", Delay, interval);
        }
        else if (attackType == RocketAttackTarget.RANDOM)
        {
            InvokeRepeating("FireRocketRandom", Delay, interval);
        }
    }

    public void LaunchRockets()
    {
        if(attackType == RocketAttackTarget.PLAYER)
        {
            InvokeRepeating("launchplayer", 0, interval/6);
        }
        
    }

    public void launchplayer()
    {
        if(nrofrockets2 <= 0)
        {
            CancelInvoke("launchplayer");
        }
            spawn.y += 2;
            GameObject go = Instantiate(fakerocket, spawn, Quaternion.identity);
            go.GetComponent<Rigidbody>().AddForce(Vector3.up * 40f, ForceMode.Impulse);
            nextSpawnLocation();
            nrofrockets2--;
    }

    public void nextSpawnLocation()
    {
        spawn = smallRocketLaunchers[smliterator].position;
        if(smliterator + 1 == smallRocketLaunchers.Count)
        {
            smliterator = 0;
        }
        else
        {
            smliterator++;
        }
    }

    public void launchrocketplayer()
    {
        Vector3 spawnlocation = smallRocketLaunchers[0].position;
        spawnlocation.y += 2;
        GameObject go = Instantiate(fakerocket, spawnlocation, Quaternion.identity);
        go.GetComponent<Rigidbody>().AddForce(Vector3.up * 30f, ForceMode.Impulse);
    }

    void FireRocketPlayer()
    {
        if (nrOfRockets <= 0)
        {
            CancelInvoke("FireRocket");
        }
        else
        {
            Vector3 spawnlocation = playerTarget.transform.position;
            spawnlocation.y += 20;
            Instantiate(rocket, spawnlocation, Quaternion.identity);
            nrOfRockets--;
        }
    }

    void FireRocketRandom()
    {
        if (nrOfRockets <= 0)
        {
            CancelInvoke("FireRocket");
        }
        else
        {
            Vector3 spawnLocation = boss.GetComponent<SpawnLocationManager>().GetRandomSpot(0, 360);
            spawnLocation.y += 20;
            Instantiate(rocket, spawnLocation, Quaternion.identity);
            nrOfRockets--;
        }
    }
}

public enum RocketAttackTarget
{
    RANDOM,
    PLAYER
}