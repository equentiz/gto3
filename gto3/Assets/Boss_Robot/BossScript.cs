﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour {

    public GameObject RocketattackPrefab;
    public bool ActivateRocketAttack = false;
    public bool ActivateRocketAttackPlayer = false;
    public rlcontroller rltop;
    public rlcontroller rlside;
    public List<Transform> rls;

	void Update ()
    {
		if(ActivateRocketAttack)
        {
            StartCoroutine(RocketAttack(100, 0.1f, 1, RocketAttackTarget.RANDOM));
            ActivateRocketAttack = false;
        }
        if (ActivateRocketAttackPlayer)
        {
            StartCoroutine(RocketAttack(10, 1, 1, RocketAttackTarget.PLAYER));
            ActivateRocketAttackPlayer = false;
        }
    }

    private IEnumerator RocketAttack(int nrOfRockets, float interval, float Delay, RocketAttackTarget attackType)
    {
        if(attackType == RocketAttackTarget.PLAYER)
        {
            yield return new WaitForSeconds(rlside.fireRockets(nrOfRockets));
        }
        if(attackType == RocketAttackTarget.RANDOM)
        {
            yield return new WaitForSeconds(rltop.fireRockets(nrOfRockets));
        }
        if(RocketattackPrefab != null)
        {
            GameObject raO = Instantiate(RocketattackPrefab);
            RocketAttack ra = raO.GetComponent<RocketAttack>();
            if (ra != null)
            {
                ra.nrOfRockets = nrOfRockets;
                ra.interval = interval;
                ra.Delay = Delay;
                ra.attackType = attackType;
                ra.BeginAttack();
            }
        }
    }

}
