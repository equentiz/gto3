﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rlcontroller : MonoBehaviour {

    Animator anim;

    public AnimationClip clip;
	// Use this for initialization
	void Start () {
        anim = this.gameObject.GetComponent<Animator>();
	}

    public float fireRockets(int nrofrockets)
    {

        anim.SetBool(anim.GetParameter(0).name, true);
        return clip.length * clip.averageDuration;

        //yield return new WaitForSeconds(clip.length)
        // anim.SetBool("OpenPods", false);
    }

}
